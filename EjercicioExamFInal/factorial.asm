	.cdecls C,LIST,"msp430.h" ; Include device header file

	.global calculateFactorial

	;.global signOfResult

;********************************************************************
calculateFactorial:
			clr 	R5		; register for storage the original number
			clr		R6		; register for storage index of multuplication
			clr 	R7		; Use to store accumulative mutiplication
			mov 	R12, R5 ; Move number to R5
			mov 	R12, R6	; index
			dec		R6
			jmp 	result



result:		mov		R5,&MPY	;multiplication of two numbers
			mov		R6,&OP2
			mov		&RESLO,R7	;store result in R11
			dec 	R6
			mov 	R7, R5
			cmp.b	#0,R6		;verify if number already covered all digits if zero flag is active
			jnz		result
			mov 	R7, R13
			nop
			ret



