#include <msp430.h> 


/**
 * main.c
 */
extern int calculateFactorial(int);

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	
	             int number = 4;

	             int result;

	             result = calculateFactorial(number);

	             return 0;
}
